/**************************************************************************** 
	This piece of javascript contains the functions that do the following:
		- authenticate the user at NERSC
		- auth user with Filepicker.io
		- authenticate with Dropbox
****************************************************************************/
var FILEPICKER_API_KEY = "Aa4AbULNlTjSBP7NaHjg2z";
// Newt urls
var newt_server="https://newt.nersc.gov";
var newt_base_path="/newt";
var newt_base_url=newt_server+newt_base_path;
var hopper_dir = "/global/homes/t/timur/";
var hopper_scratch = "/global/scratch/sd/jmustafa/phd4/test_in/";
var NERSC_URL = "http://portal.nersc.gov/project/ucbcmt/outbox";
//var hopper_scratch = "/global/scratch/sd/timur/tmp/";
//var USERNAME = ""
//var PASSWORD = "";

var DROPBOX_API_KEY    = "zpx6bske4xk4zal";
var DROPBOX_API_SECRET    = "pye6cp72gta93f6";
var Dbclient = new Dropbox.Client({
  		key: "oIYjtUi0ocA=|fXGYONJtG84zLHHB8Tv6/f2noUiqdo2YKVnyJXwWYw==", sandbox: true
});
var myDriver = new Dropbox.Drivers.Redirect({
//rememberUser: true
})

function SendData (db_json, filepicker_files) {
	var ac_json = ImageAction();
	 //console.log("fpfiles = " + fpfiles);
	var fp_json = JSON.stringify(filepicker_files);
	var json = {
		"select" : ac_json,
		"dropbox" : db_json,
		"filepicker": filepicker_files
	};
	console.log("JSON = " + JSON.stringify(json));
	jqlogin();
	jqput(JSON.stringify(json));
}

function Filepicker_pickMultiple(){
	console.log("In Filepicker_pickMultiple");
	filepicker.setKey(FILEPICKER_API_KEY);
	filepicker.pickMultiple(function(fpfiles){
	    //Dropbox_auth(SendData, Dbclient);
	   	SendData("{}",fpfiles);
	});
}

function jqlogin(){
	$.newt_ajax({type: "GET",
    	url: "/login",
    	success: function(res){
    	    // res is { username: 'xxxxx', auth: true|false }
    	    $("#auth").html("Login Status: "+res.auth);
    	},
	});
}
/*
function jqlogout(){
	$.newt_ajax({type: "GET",
    	url: "/logout",
    	success: function(res){
    	    // res is { username: 'xxxxx', auth: true|false }
    	    $("#auth").html("Logout Status: "+res.auth);
    	},
	});
}
*/
function jqget(){
	$.newt_ajax({type: "GET",
    	url: "/file/carver/etc/motd?view=read",
    	success: function(res){
    	    // res contains raw file data
    	    $('#file').html(res);
    	    console.log(res);
    	},
	});
}

function jqput(string){
	$.newt_ajax({type: "PUT",
    	url: "/file/hopper/" + hopper_scratch,
    	data: string,
    	success: function(res){
    	    // res contains raw file data
    	    $('#file').html(res);
    	    console.log(res);
    	    window.location = NERSC_URL;
    	    //window.alert("Your files are at this url: " + NERSC_URL);
    	},
	});
}

/*****************
 DROPBOX AUTH STUFF
*****************/

function Dropbox_auth(callback, client){

	// Check if we are inside the auth process for Dropbox - TODO: make it less hacky
	if (document.URL.indexOf('oauth') < 0){

		console.log("IN Dropbox_auth " + "Calling authDriver");

		client.authDriver(myDriver);
	}
	else {
		console.log("IN Dropbox_auth " + "Calling authenticate");
		Dropbox_getAccessToken(callback, client);
	}


}

function Dropbox_getAccessToken(callback, client) {

	client.authenticate(function(error, client) {
	  if (error) {
	    console.log("ERROR : Dropbox authentication went wrong");
	    return showError(error);
	  }

	  var data = {
	  	"oauth_token" : getQueryVariable('oauth_token', document.URL),
	  	"dboauth_token" : getQueryVariable('dboauth_token', document.URL),
	  	"uid" : getQueryVariable('uid', document.URL),
	  	"api-key" : DROPBOX_API_KEY,
	  	"api-secret" : DROPBOX_API_SECRET
	  };

	  console.log("AUTHORIZED for Dropboxing " + data.uid);

	  callback(data, client);

	});
}

function CheckDropboxAuth(){
	if (document.URL.indexOf('oauth') > 0){
		Dropbox_auth(SendData, Dbclient);
	}
}

var showError = function(error) {
  switch (error.status) {
  case Dropbox.ApiError.INVALID_TOKEN:
    // If you're using dropbox.js, the only cause behind this error is that
    // the user token expired.
    // Get the user through the authentication flow again.
    break;

  case Dropbox.ApiError.NOT_FOUND:
    // The file or folder you tried to access is not in the user's Dropbox.
    // Handling this error is specific to your application.
    break;

  case Dropbox.ApiError.OVER_QUOTA:
    // The user is over their Dropbox quota.
    // Tell them their Dropbox is full. Refreshing the page won't help.
    break;

  case Dropbox.ApiError.RATE_LIMITED:
    // Too many API requests. Tell the user to try again later.
    // Long-term, optimize your code to use fewer API calls.
    break;

  case Dropbox.ApiError.NETWORK_ERROR:
    // An error occurred at the XMLHttpRequest layer.
    // Most likely, the user's network connection is down.
    // API calls will not succeed until the user gets back online.
    break;

  case Dropbox.ApiError.INVALID_PARAM:
  case Dropbox.ApiError.OAUTH_ERROR:
  case Dropbox.ApiError.INVALID_METHOD:
  default:
    // Caused by a bug in dropbox.js, in your application, or in Dropbox.
    // Tell the user an error occurred, ask them to refresh the page.
  }
};

function getQueryVariable(variable, query) {
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}


function ImageAction(){
	var e = document.getElementById('selector-action');
    var selector = e.options[e.selectedIndex].value;
	var e1 = document.getElementById('selector-action');
    var selector1 = e1.options[e.selectedIndex].value;
    var action = "resize";
    var type   = "type1";

    switch (e) {
    case 1:
      // If you're using dropbox.js, the only cause behind this error is that
      // the user token expired.
      // Get the user through the authentication flow again.
      action = "resize";
      break;
  
      case 2:
        // The file or folder you tried to access is not in the user's Dropbox.
        // Handling this error is specific to your application.
        action = 'filter';
        break;
  
      case 3:
        // The user is over their Dropbox quota.
        // Tell them their Dropbox is full. Refreshing the page won't help.
        action = 'compress';
        break;
  
      case 4:
        // Too many API requests. Tell the user to try again later.
        // Long-term, optimize your code to use fewer API calls.
        action = 'sexify';
        break;
  
      default:
	}


	switch (e1) {
    case 1:
      // If you're using dropbox.js, the only cause behind this error is that
      // the user token expired.
      // Get the user through the authentication flow again.
      type = "type1";
      break;
  
      case 2:
        // The file or folder you tried to access is not in the user's Dropbox.
        // Handling this error is specific to your application.
        type = 'type2';
        break;
  
      case 3:
        // The user is over their Dropbox quota.
        // Tell them their Dropbox is full. Refreshing the page won't help.
        type = 'type3';
        break;
  
      default:
	}
	console.log("action =" + action + "type =" + type);

	return {
		"action" : action,
		"type" : type
	}
}
/*
function nersc_login(){
	var url = newt_base_url + "/login";
	var params = "username="+ USERNAME +"&password=" + PASSWORD;
	var http = new XMLHttpRequest();


	http.open("POST", url, true);
	
	//Send the proper header information along with the request
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//http.setRequestHeader("Content-length", params.length);
	//http.setRequestHeader("Connection", "close");
	
	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			alert(http.responseText);
		}
	}
	http.send(params);
}


function nersc_logout(theUrl){
	var url = newt_base_url + "/logout";
	var http = new XMLHttpRequest();

	http.open("GET", url, true);
	
	//Send the proper header information along with the request
	//http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//http.setRequestHeader("Content-length", params.length);
	//http.setRequestHeader("Connection", "close");
	
	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			alert(http.responseText);
		}
	}
	http.send(null);
}

function nersc_list(){
	var url = newt_base_url + "/file/hopper";
	var http = new XMLHttpRequest();

	http.open("GET", url, true);
	
	//Send the proper header information along with the request
	//http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//http.setRequestHeader("Content-length", params.length);
	//http.setRequestHeader("Connection", "close");
	
	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			alert(http.responseText);
		}
	}
	http.send(null);
}

function nersc_create(theUrl){
	var url = newt_base_url + "/file/hopper/";
	var params = JSON_example;
	var http = new XMLHttpRequest();

	http.open("PUT", url, true);
	
	//Send the proper header information along with the request
	//http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//http.setRequestHeader("Content-length", params.length);
	//http.setRequestHeader("Connection", "close");
	
	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			alert(http.responseText);
		}
	}
	http.send(null);
}
*/