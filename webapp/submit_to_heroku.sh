#!/bin/sh
HEROKU_REPO="git@heroku.com:phd4.git"
DATE=`date`
# Initialize an empty repo and sync it with heroku
git init
git remote add heroku $HEROKU_REPO
git pull heroku master
git add . -A
git commit -m "Commit at $DATE"
git push heroku master
rm -rf .git
git remote rm heroku $HEROKU_REPO