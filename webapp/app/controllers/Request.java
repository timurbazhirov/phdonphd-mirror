package controllers;

import models.*;

import play.libs.F.Function;
import play.libs.*;
import play.libs.WS;
import play.mvc.*;
import play.Logger;
import play.mvc.Http.Session;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;

import com.feth.play.module.pa.PlayAuthenticate;
import com.feth.play.module.pa.user.AuthUser;

import views.html.*;

public class Request extends Controller {

  private static final String serverIP = "http://54.244.114.238:8182"; // /graphs/kabandb-test";
  public static String requestResponse = "HAVEN'T SENT YET";
  
  public static User getLocalUser(final Session session) {
		final AuthUser currentAuthUser = PlayAuthenticate.getUser(session);
		final User localUser = User.findByAuthUserIdentity(currentAuthUser);
		return localUser;
  }

  @Restrict(@Group(Application.USER_ROLE))
  public static Result renderResponse() {
      String feedUrl = serverIP + "/graphs/kabandb-test";
	    final User localUser = getLocalUser(session());

      return async(
          WS.url(feedUrl).get().map(
          new Function<WS.Response, Result>() {
              public Result apply(WS.Response response) {
                return ok(getresponse.render(localUser, "Feed answer:" + response.asJson().toString()));
              }
          }
        )
      );
  }

  public static Result getResponse(String path) {
      String feedUrl = serverIP + path;
      final User localUser = getLocalUser(session());
      Logger.info("in GET RESPONSE");

      F.Promise<WS.Response> futureResponse = WS.url(feedUrl).get();

      F.Promise<Result> futureResult = futureResponse.map(

            new Function<WS.Response, Result>() {
                public Result apply(WS.Response response) {
                  requestResponse = response.asJson().toString();
                  return ok(getresponse.render(localUser, "Feed answer:" + response.asJson().toString()));
                }
            }
      );
      return async(futureResult);
  }
  
}