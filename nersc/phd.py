#!/usr/bin/env python
from __future__ import division
import os
import time
import argparse
import tarfile
import json
from dbox import post
from PIL import Image, ImageFilter
try:
    from mpi4py import MPI
    mpi_enabled = True
except:
    mpi_enabled = False

if mpi_enabled:
    mpi_comm = MPI.COMM_WORLD
    mpi_size = mpi_comm.Get_size()
    mpi_rank = mpi_comm.Get_rank()
else:
    mpi_size = 1
    mpi_rank = 0
    
class Timer():
    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.interval = self.end - self.start
        print self.interval

def make_thumb(file, out_dir, thumb_size=(60,60)):
    img = Image.open(file)
    img.thumbnail(thumb_size)
    img.save(os.path.join(out_dir, os.path.basename(file)))

def filter(file, out_dir, filter):
    img = Image.open(file)
    format = img.format
    # if format == 'GIF':
        # continue
    out = img.filter(filter)
    out.save(os.path.join(out_dir, os.path.basename(file)))

def resize(files):
    pass

def blackwhite(file, out_dir):
    img = Image.open(file)
    format = img.format
    out = img.convert('L')
    out.save(os.path.join(out_dir, os.path.basename(file)))

def compress(files):
    pass

def get_files(data_dir):
    files = []
    for item in os.listdir(data_dir):
        path = os.path.join(data_dir, item)
        if os.path.isfile(path) and path.split('.')[-1].upper() in ['JPG', 'JPEG']:
            files.append(path)

    if len(files) == 0:
        return None

    return files

def print_header():
    print '='*60
    print ''
    print 'PhDs@phd4'
    print ''
    print 'author: Jamal I. Mustafa  <jimustafa@gmail.com>'
    print 'author: Timur T. Bazhirov <timur.bazhirov@gmail.com>'
    print ''
    print '='*60

def main(args):
    if mpi_rank == 0:
        files = get_files(args.data_dir)
        if files == None:
            print 'ERROR: no files found'
            return
    else:
        files = None
    if mpi_enabled:
        files = mpi_comm.bcast(files, root=0)

    files = files[:500]
    nfiles = len(files)

    if mpi_rank == 0:
        print 'Processing', nfiles, 'files'

    n = nfiles // mpi_size
    if nfiles % mpi_size != 0:
        n += 1
    my_files = [files[i:i+n] for i in range(0, nfiles, n)][mpi_rank]

    if args.verbose > 0:
        print mpi_rank, len(my_files)
    # f = make_thumb
    f = filter
    # f = blackwhite

    for (i, file) in enumerate(my_files):
        # f(file, args.out_dir)
        f(file, args.out_dir, ImageFilter.BLUR)

    if mpi_rank == 0:
        print args.json_path
        if args.tar:
            out_dir = os.path.abspath(args.out_dir)
            tar_path = os.path.join(os.path.dirname(out_dir), os.path.basename(out_dir).strip('/')+'.tar')
            print tar_path
            with tarfile.open(tar_path, 'w') as tar:
                os.chdir(args.out_dir)
                tar.add('.')
            if args.post:
                with open(args.json_path, 'r') as f:
                    json_data = json.load(f)
                tokens = [json_data['dropbox']['oauth_token'], json_data['dropbox']['oauth_token']]
                tokens[0] = tokens[0].encode('ascii')
                tokens[1] = tokens[1].encode('ascii')
                print tokens
                post(tar_path, tokens)
        
if __name__=='__main__':
    if mpi_rank == 0:
        print_header()
    parser = argparse.ArgumentParser()
    parser.add_argument('json_path')
    parser.add_argument('data_dir')
    parser.add_argument('out_dir')
    parser.add_argument('--tar', action='store_true')
    parser.add_argument('--post', action='store_true')
    parser.add_argument('--verbose', '-v', action='count')
    args = parser.parse_args()

    if mpi_enabled:
        t0 = MPI.Wtime()
        main(args)
        if mpi_rank == 0:
            print MPI.Wtime() - t0
    else:
        with Timer():
            main(args)
