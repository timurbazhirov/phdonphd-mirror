#!/usr/bin/env python
import argparse
import Queue
import threading
import urllib2
import json
import os

class Downloader(threading.Thread):
    def __init__(self, queue, dst):
        threading.Thread.__init__(self)
        self.queue = queue
        self.dst = dst

    def run(self):
        while True:
            url = self.queue.get()
            download_file(url, self.dst)
            self.queue.task_done()

def download_file(url, dst):
    s = url.split('/')[-1]
    dst_path = os.path.join(dst, s+'.jpeg')
    with open(dst_path, 'wb') as f:
        f.write(urllib2.urlopen(url).read())

def main(args):
    with open(args.json_file) as f:
        json_data = json.load(f)

    if args.threads > 1:
        queue = Queue.Queue()
        for i in range(args.threads):
            t = Downloader(queue, args.data_dir)
            t.daemon = True
            t.start()

        for obj in json_data['filepicker']:
            queue.put(obj['url'])
        queue.join()
    else:
        for obj in json_data:
            download_file(obj['url'], args.data_dir)

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('json_file')
    parser.add_argument('data_dir')
    parser.add_argument('--threads', type=int, default=1)
    args = parser.parse_args()

    main(args)
