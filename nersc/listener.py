#!/usr/bin/env python
import os
import time
import argparse
import tempfile
import ConfigParser

root_path = os.path.dirname(os.path.abspath(__file__))
dl_script_path = os.path.join(root_path, 'dl.py')

def sub_serial(json_path, data_dir, out_dir):
    cmd = ' '.join([os.path.join(root_path, 'phd.py'), json_path, data_dir, out_dir, '--tar', '--post'])
    print cmd
    os.system('mkdir '+ out_dir)
    os.system(cmd)

def gen_job_pbs(data_dir, out_dir, job_dir):
    with open(os.path.join(root_path, 'job.pbs'), 'r') as f:
        job_script = f.readlines()

    job_script += ['\n', ' '.join(['mkdir', out_dir])]
    cmd = ' '.join(['aprun', '-n', '24', '-a xt', os.path.join(root_path, 'phd.py'), data_dir, out_dir])
    job_script += ['\n', cmd]

    job_script_path = os.path.join(job_dir, 'job.'+os.path.basename(data_dir))
    with open(job_script_path, 'w') as f:
        f.writelines(job_script)

    os.chdir(job_dir)
    os.system(' '.join(['qsub', job_script_path]))

def sub_int_job(data_dir, out_dir):
    cmd = ' '.join(['aprun', '-n', '1', '-a xt', os.path.join(root_path, 'phd.py'), data_dir, out_dir])
    print 'Running', cmd
    os.system(cmd)

def print_header():
    print '='*60
    print ''
    print 'PhDs@phd4'
    print ''
    print 'Started the "listener"'
    print ''
    print '='*60

def start(args):
    config = ConfigParser.ConfigParser()
    config.read(os.path.join(root_path, 'phd4.cfg'))

    in_dir = config.get('Params', 'in_dir')
    tmp_data_dir = config.get('Params', 'data_dir')
    out_dir = config.get('Params', 'out_dir')
    job_dir = config.get('Params', 'job_dir')

    before = dict([(f, None) for f in os.listdir(in_dir)])
    while 1:
        try:
            time.sleep(5)
            after = dict([(f, None) for f in os.listdir (in_dir)])
            added = [f for f in after if not f in before]
            removed = [f for f in before if not f in after]
            before = after

            if added:
                for f in added:
                    json_path = os.path.join(in_dir, f)
                    data_dir = tempfile.mkdtemp(dir=tmp_data_dir)
                    tmp_dir_name = os.path.basename(data_dir)
                    # print 'Processing', json_path, 'into', data_dir
                    print 'Processing %s' % json_path
                    print 'Downloading to %s...' % data_dir,
                    os.system(' '.join([dl_script_path, json_path, data_dir, '--threads 4']))
                    print 'DONE'
                    if args.auto_submit:
                        print 'Submitting job...',
                        if args.submit_type.upper() == 'SERIAL':
                            sub_serial(json_path, data_dir, os.path.join(out_dir, tmp_dir_name))
                        else:
                            pass
                            #gen_job_pbs(data_dir, os.path.join(out_dir, tmp_dir_name), job_dir)
                            # sub_int_job(data_dir, out_dir)
        except KeyboardInterrupt:
            print ''
            print 'Stopped the "listener"'
            return

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--auto-submit', action='store_true')
    parser.add_argument('--submit-type')
    args = parser.parse_args()

    print_header()

    start(args)
